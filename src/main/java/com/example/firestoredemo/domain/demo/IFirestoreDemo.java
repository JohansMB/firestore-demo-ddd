package com.example.firestoredemo.domain.demo;

import java.util.concurrent.ExecutionException;

public interface IFirestoreDemo {

    Integer createDemo(FirestoreDemo demo) throws ExecutionException, InterruptedException;

    FirestoreDemo getDemo(Integer id) throws ExecutionException, InterruptedException;

}
