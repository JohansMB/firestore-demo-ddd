package com.example.firestoredemo.domain.demo;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FirestoreDemo {

    private Integer       id;
    private String     name;

}
