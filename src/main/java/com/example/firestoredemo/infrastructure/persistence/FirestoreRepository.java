package com.example.firestoredemo.infrastructure.persistence;

import com.example.firestoredemo.config.FirestoreConfig;
import com.example.firestoredemo.domain.demo.FirestoreDemo;
import com.example.firestoredemo.domain.demo.IFirestoreRepository;
import com.example.firestoredemo.domain.demo.NotFoundError;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Service()
public class FirestoreRepository implements IFirestoreRepository {

    private final CollectionReference collection;

    FirestoreRepository(FirestoreConfig firestoreConfig) {
        this.collection = firestoreConfig.db.collection("firestore");
    }

    @Override
    public Integer save(FirestoreDemo firestoreDemo) throws ExecutionException, InterruptedException {
        ObjectMapper oMapper = new ObjectMapper();
        Map<String, Object> mapper = oMapper.convertValue(firestoreDemo, Map.class);
        ApiFuture<WriteResult> result =  collection.document("demo").set(mapper);
        System.out.println("Update time : " + result.get().getUpdateTime());
        return 1;
    }

    @Override
    public FirestoreDemo get(Integer id) throws ExecutionException, InterruptedException {
        ApiFuture<QuerySnapshot> query = collection.whereEqualTo("id", id).get();
        List<QueryDocumentSnapshot> documents = query.get().getDocuments();
        if (documents.size() == 0)
            throw new NotFoundError("Not Found Firestore Demo");
        return documents.get(0).toObject(FirestoreDemo.class);
    }
}
