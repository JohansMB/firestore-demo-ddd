package com.example.firestoredemo.domain.demo;

import java.util.concurrent.ExecutionException;

public interface IFirestoreRepository {
    Integer save(FirestoreDemo firestoreDemo) throws ExecutionException, InterruptedException;
    FirestoreDemo get(Integer id) throws ExecutionException, InterruptedException;
}
