package com.example.firestoredemo.application.controller.demo;

import com.example.firestoredemo.domain.demo.FirestoreDemo;
import com.example.firestoredemo.domain.demo.IFirestoreDemo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.ExecutionException;

@RestController
public class FirestoreController {

    @Autowired()
    IFirestoreDemo firestoreDemo;

    @PostMapping("/demo")
    @ResponseStatus(HttpStatus.CREATED)
    public Integer createDemo(@RequestBody FirestoreDemo demo) throws ExecutionException, InterruptedException {
        return firestoreDemo.createDemo(demo);
    }

    @GetMapping("/demo/{id}")
    @ResponseStatus(HttpStatus.OK)
    public FirestoreDemo createDemo(@PathVariable("id") Integer id) throws ExecutionException, InterruptedException {
        return firestoreDemo.getDemo(id);
    }
}
