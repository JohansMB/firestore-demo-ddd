package com.example.firestoredemo.domain.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;

@Service()
public class FirestoreDemoService implements IFirestoreDemo{

    @Autowired()
    IFirestoreRepository firestoreRepository;

    @Override
    public Integer createDemo(FirestoreDemo demo) throws ExecutionException, InterruptedException {
        return firestoreRepository.save(demo);
    }

    @Override
    public FirestoreDemo getDemo(Integer id) throws ExecutionException, InterruptedException {
        return firestoreRepository.get(id);
    }
}
